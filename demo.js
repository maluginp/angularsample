(function(){
	angular
		.module('App', [])
		.controller('ParentController', ParentController)
		.controller('ChildController', ChildController);

		ParentController.$inject = ['$scope','$log'];
		function ParentController($scope, $log) {
			$log.debug('ParentController');
			$scope.passedParam = 'hahaha';
			$scope.parentItem = "abc";

			$scope.init = function(type, type2) {
				$log.debug('Parent init type:', type,'Type2:',type2);
			};

			$scope.$on('UploadFileFinishedEvent', function(event, errors, file){
				$log.debug('Catch UploadFileFinished Event', 'Errors:', errors, 'Filename:', file);
			});

			$scope.create = function() {
				$log.debug('Create button was clicked', 'ParentItem', $scope.parentItem);

				$scope.$broadcast('UploadFileEvent',['file']);
			};

			$scope.parentFunc = function (source) {
				$log.debug("[Parent] Called from:", source, 'ParentItem:',$scope.parentItem);
			};

		}

		ChildController.$inject = ['$scope', '$log'];
		function ChildController($scope, $log) {
			$log.debug('ChildController');
			var vm = this;
			


			$scope.init = function(type, type2) {
				$log.debug('Child Init type:', type,'Type2:',type2);
				
			};

			$scope.$on('UploadFileEvent', function(event, file){
				$log.debug('Catch UploadFile Event', 'Filename', file);
				vm.uploader.upload();
			});

			$scope.parentFunc('From child');
			$log.debug('[Child] ParentItem', $scope.parentItem);
			$scope.parentItem = 'cba';
			$log.debug('[Child] ($scope.parentItem = cba) ParentItem=', $scope.parentItem);
			$scope.parentFunc('From child');
			$scope.$parent.parentItem = 'cccc';
			$log.debug('[Child] ($scope.$parent.parentItem = cccc) ParentItem=', $scope.parentItem);
			$scope.parentFunc('From child');

			vm.uploader = new function() {
				this.upload = function() {
					$log.debug('Upload');
					this.uploadFinish();
				};

				this.uploadFinish = function() {
					$log.debug('Upload finish');

					$scope.$emit('UploadFileFinishedEvent', null , ['filename_fffaf', 'ababa', 'asdas']);
				}
			};
		}
;
})();
